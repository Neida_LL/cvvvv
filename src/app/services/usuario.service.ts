import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { PublicI, PublicIU, UsuarioI } from 'src/app/interface/usuario.interface';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  usuarioGlobal!:UsuarioI;


  listaDeUsuarios: UsuarioI[] = [
    {usuario: '@Soliz', password: 'Juan',fecha: '2022-06-28T19:10',ultima_coneccion:'2022-06-28T19:30',cant_like:1,cant_dislike:2,lista_public:[
      {id: "sTrA", usuario: "@Soliz", fecha: "Thu Jul 14 2022 08:17:06 GMT-0400 (hora de Bolivia)",titulo:  "El café Solventado",contenido:"<h1 style=\"text-align:center\"><strong>Nuevas Soluciones</strong> </h1><p><img src=\"https://us.123rf.com/450wm/vizafoto/vizafoto1912/vizafoto191200404/147404614-caf%C3%A9-en-una-taza-y-un-platillo-sobre-un-fondo-antiguo-.jpg?ver=6\" alt=\"new\" title=\"new\" width=\"675px\">    </p><p><em><strong>Lo bueno del café soleado es que siempre esta mas esquisisto que otros cafes.</strong></em></p>",cant_like: 0,cant_dislike: 0,lista_comentarios: []},
      {id: "dfhI", usuario: "@Soliz", fecha: "Thu Jul 14 2022 14:07:31 GMT-0400 (hora de Bolivia)",titulo: "Presentaciones",contenido:"<blockquote><h2 style=\"text-align:center\"><strong>Nueva presentación del nuevo Café</strong></h2><h2 style=\"text-align:center\"><strong><img src=\"https://www.thegourmetjournal.com/wp-content/uploads/2021/03/Granos-de-cafe-tostados.jpg\" alt=\"Café, seis granos del mundo | The Gourmet Journal: Periódico de Gastronomía\" width=\"646px\"></strong></h2></blockquote><p><em><strong>La <span style=\"color:#fbca04;\">opinión publica</span> a mejorado las nuevas presentaciones de manera de entrega que se ven <span style=\"color:#fbca04;\">espectaculares.</span></strong></em></p>",cant_like: 3,cant_dislike: 0,lista_comentarios: []}
    ]},
    

    {usuario: '@morita', password: 'Juan',fecha: '2022-06-28T19:15',ultima_coneccion:'Sat Jul 09 2022 11:30:41 GMT-0400 (hora de Bolivia)',cant_like:1,cant_dislike:2,lista_public:[
      {id: "tkP4", usuario: "@morita", fecha: "Thu Jul 14 2022 16:07:31 GMT-0400 (hora de Bolivia)",titulo:  "El Capuchinno",contenido:"<h1 style=\"text-align:center\"><em><strong><span style=\"color:#d93f0b;\">El magnifico Café Capuchino</span></strong></em></h1><p style=\"text-align:center\"><img src=\"https://www.hogarmania.com/archivos/201301/cafe-formas-de-preparar-xl-668x400x80xX.jpg\" alt=\"Café, diferentes formas de prepararlo - Hogarmania\" width=\"265px\"> <img src=\"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQDnAyugtZYNT9KOEMG7338MJ_w8zR0T9Wz-c-ZY6A1Zj9XeiF_PiNt5_QhNCU7Htrfmgc&amp;usqp=CAU\" alt=\"Divina presentación cuando pedís un café! EL salchichón de chocolate lo  más! - Picture of Heladeria del Abuelo, Montevideo - Tripadvisor\" width=\"256px\">   <img src=\"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSz8DQewJuIZH0GXM2DjNJt2LSlwgJYESjBDQ&amp;usqp=CAU\" alt=\"como hacer capuchino\" width=\"259px\"><img src=\"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT19NTK4Z_Au85g7Yz_4B8tXxsnPWjWv6-e2w&amp;usqp=CAU\" alt=\"Como Hacer un Capuchino en Casa - Cultura Cafeina\"></p><p></p><h3><strong>A que no se imaginan estas espectacuklare delicias en sus hogares por las mañanas seria genial.</strong></h3><ul><li><p><strong>Son magnificas sabrosidades</strong></p></li><li><p><strong>Excelentes en hierro</strong></p></li><li><p><strong>Eso si,<span style=\"color:#b60205;\"> no son aptos para niños.</span></strong></p></li></ul>",cant_like: 1,cant_dislike: 0,lista_comentarios: []}
    ]},
   
    {usuario: 'Sol_nueva',password: 'Juan',fecha: '2022-05-28T03:30',ultima_coneccion:'Sat Jul 09 2022 11:30:41 GMT-0400 (hora de Bolivia)',cant_like:1,cant_dislike:2,lista_public:[
      {id: "r2Nv", usuario: "Sol_nueva", fecha: "Thu Jul 10 2022 14:07:31 GMT-0400 (hora de Bolivia)",titulo:"La cosecha del Café  ",contenido: "<blockquote><h1 style=\"text-align:center\"><strong><span style=\"color:#d93f0b;\">El asombroso Café</span></strong></h1><p style=\"text-align:center\"><span style=\"color:#d93f0b;\">lo nuevooo</span></p><h3><img src=\"https://www.bonka.es/themes/custom/bonka/img/baya-cafe-roja.jpg\" alt=\"La recolección del café | Bonka\" width=\"569px\"></h3><h3><strong>“En esta sección se podrá visualizar todo lo que asemeja la <span style=\"color:#0e8a16;\">naturaleza</span> ,el arte de mostrar el café.“</strong></h3></blockquote>",cant_like: 1,cant_dislike: 0,lista_comentarios: []}
    ]}
  ];

  constructor(private router:Router) { 
    this.cargarDatos();
  }

  
  //------Usaurio Global--------------------------------------
  setUsuarioGlobal(usuario:String){
    let user = this.buscarUsuario(usuario);
    this.usuarioGlobal = user;
    localStorage.setItem('usuarioGlobal',JSON.stringify(this.usuarioGlobal));
  }
  
  
  getUserG():UsuarioI{
      let guardados = localStorage.getItem('usuarioGlobal');
      this.usuarioGlobal = JSON.parse(guardados || '{}');
    return this.usuarioGlobal;
    
  }
  
  dropUsuarioGlobal(){
    localStorage.removeItem("usuarioGlobal");
    this.setUsuarioGlobal('');
  }
  
  
  getUserPass(user:String,pass:any){
    return this.listaDeUsuarios.find(element => element.password == pass && element.usuario == user) || {} as UsuarioI;
  }
  //----------------------Lista  De Usuarios----------------------------
  getListUsers(): UsuarioI[] {

    //slice retorna  una copia del array
    return this.listaDeUsuarios.slice();
  }


  eliminarListUsers(id:String){
    this.listaDeUsuarios =this.listaDeUsuarios.filter(data => {
      return data.usuario!==id; 
    })
  }

  agregarUserList(usuario:UsuarioI){
    this.listaDeUsuarios.unshift(usuario);
  }

  buscarUsuario(id: String): UsuarioI{
    //o retorna un json {} vacio
    return this.listaDeUsuarios.find(element => element.usuario === id) || {} as UsuarioI;
  }


  // modificarUsuario(user: UsuarioI){
  //   this.eliminarListUsers(user.id);
  //   this.agregarUserList(user);
  // }

  listaNamesUsers(): String []{
    this.cargarDatos();
    let list:String[] = [];
    this.listaDeUsuarios.forEach(e => list.push(e.usuario));
    return list;
  }


  cargarDatos(){

    //-------------------Users-----------------------------
    if (!localStorage.getItem("listaUsers")) {

      this.cargarDatosDeLocal();

    } else{
      //Obtiene del local y carga datos
      //localStorage.setItem("listaAgenda", JSON.stringify( this.listUsuarios));
      let guardados = localStorage.getItem('listaUsers');
      this.listaDeUsuarios = JSON.parse(guardados || '{}');
    }


    if (!localStorage.getItem("usuarioGlobal")) {
      let guardados = localStorage.getItem('usuarioGlobal');
      this.usuarioGlobal = JSON.parse(guardados || '{}');
    } 

  }

  cargarDatosDeLocal(){
    //carga el local con datos y vuelve a treaerlos
      localStorage.setItem("listaUsers", JSON.stringify( this.listaDeUsuarios));
      let guardados = localStorage.getItem('listaUsers');
      this.listaDeUsuarios = JSON.parse(guardados || '{}');
  }


  //-----------------Ingresar Un Nuevo post---------------------
  agregarPost(post:PublicI){
    let user = this.buscarUsuario(post.usuario);
    user.lista_public.push(post);
    this.eliminarListUsers(user.usuario);
    this.agregarUserList(user);
    this.cargarDatosDeLocal();
    this.setUsuarioGlobal(user.usuario);
  }


  eliminarPost(id:string){
    this.getUserG();
    console.log(this.usuarioGlobal);
    
    this.usuarioGlobal.lista_public = this.usuarioGlobal.lista_public.filter(e =>{
      return e.id!== id;
    });
    this.eliminarListUsers(this.usuarioGlobal.usuario);
    this.agregarUserList(this.usuarioGlobal);
    this.cargarDatosDeLocal();
    this.setUsuarioGlobal(this.usuarioGlobal.usuario);
  }




  //-----------------Ingresar Un comentario---------------------
  agregarPostPorComent(post:PublicI){
    let user = this.buscarUsuario(post.usuario);
    console.log(post);
    
    user.lista_public = user.lista_public.filter(data => {
      console.log(data.id);
      console.log(post.id);
      
      return data.id!==post.id; 
    })
    console.log(user.lista_public);

    user.lista_public.push(post);
    this.eliminarListUsers(user.usuario);
    this.agregarUserList(user);
    this.cargarDatosDeLocal();
    //actualizar al usuarioG ya que el comentario puede ser a si mismo
    //y si no lo es no pasa nada si lo actualizamos
    let userg = this.getUserG();
    this.setUsuarioGlobal(userg.usuario); 
  }
  
}
