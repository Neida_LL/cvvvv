import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { Editor, Toolbar } from 'ngx-editor';
import { PublicI, PublicIU, UsuarioI } from 'src/app/interface/usuario.interface';
import { PublicacionService } from 'src/app/services/publicacion.service';

@Component({
  selector: 'app-ver-post',
  templateUrl: './ver-post.component.html',
  styleUrls: ['./ver-post.component.css']
})
export class VerPostComponent implements OnInit {


  @ViewChild("userHtml", { static: false }) userHtml!:any;
  
  userG!:UsuarioI;
  publicacion!:PublicI;
  
  
  title = 'texto-enriquecido';
 
  nuevo = '';
  form!:FormGroup;


  editor:Editor;
  toolbar:Toolbar = [
     ['bold', 'italic'],
     ['underline', 'strike'],
     ['code', 'blockquote'],
     ['ordered_list', 'bullet_list'],
     [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
     ['link', 'image'],
     ['text_color', 'background_color'],
     ['align_left', 'align_center', 'align_right', 'align_justify'],

  ];


  constructor(private _publicService:PublicacionService,
              private fb:FormBuilder
              ) { 
    this.editor = new Editor();
    this.cargarPublic();
    this.cargarForm();
  }

  ngOnInit(): void {
  }
  
  cargarForm(){
    this.form = this.fb.group({
      contenido : ['',[Validators.required,Validators.minLength(2)]]
    })
  }

  
  
  cargarPublic(){
    let guardados = localStorage.getItem('verPublish');
    this.publicacion = JSON.parse(guardados || '{}');;
    console.log(this.publicacion);
    console.log(new Date()+'');
    
    
      let guardados1 = localStorage.getItem('usuarioGlobal');
      this.userG = JSON.parse(guardados1 || '{}');
      console.log(this.userG);
      
     
  }
  
  Guardar():void{
    let date= new Date();
    console.log(date);
    
    let comentario:PublicIU ={
      usuario:this.userG.usuario,
      fecha:''+date,
      contenido :this.form.value.contenido,
      cant_like :0,
      cant_dislike :0,
      
    } 
    console.log(this.form.value.contenido,comentario.usuario);
    this.publicacion.lista_comentarios.push(comentario);
    this._publicService.agregarComent(this.publicacion);
    // this._publicService.cargarDatos();
    let publish:PublicI = this._publicService.buscarPublic(this.publicacion.id);
    localStorage.setItem("verPublish", JSON.stringify(publish));
    this.cargarPublic();
    this.form.reset();
    // let ne = this.html;
    //  localStorage.setItem('Contenido',ne)

    // this.html = '<b>Hola</b>'+localStorage.getItem('Contenido');
    // console.log(this.html);
    
  }

  like(){
    
  }

  dislike(){}
}
