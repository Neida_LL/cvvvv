import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Editor, Toolbar } from 'ngx-editor';
import { PublicI, UsuarioI } from 'src/app/interface/usuario.interface';
import { PublicacionService } from 'src/app/services/publicacion.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  @ViewChild("userHtml", { static: false }) userHtml!:any;

  user!:UsuarioI;
  listPublic:PublicI[]=[];
  publico!:PublicI;
  constructor(private _publicService:PublicacionService,
              private _userService:UsuarioService,
              private _snackbar:MatSnackBar, 
              private router:Router) { 
              this.cargarDatos();  
              console.log(this.listPublic);
              this.publico = this.listPublic[0];          
              }

  ngOnInit(): void {
  }

  cargarDatos(){
    this.listPublic = this._publicService.getListPublic();
  }

  verPost(id:any){
   if (Object.keys(this._userService.getUserG()).length === 0 ) {
    this.router.navigate(['inicio']);
      this._snackbar.open('Por favor iniciar Sesion ', '', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
        });
   } else {
    console.log(this._publicService.buscarPublic(id));
    let publish:PublicI = this._publicService.buscarPublic(id);
    console.log(publish);
    
    localStorage.setItem("verPublish", JSON.stringify(publish));
    console.log(id);
    
    this.router.navigate(['inicio/ver-post']);
   }

  }
 

  like(){
  
  }


  dislike(){

  }
}
