import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent, SafeHtmlPipe } from './dashboard.component';
import { InicioComponent } from './inicio/inicio.component';
import { NavbarComponent } from './navbar/navbar.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { ReportesComponent } from './reportes/reportes.component';
import { SharedModule } from '../shared/shared.module';
import { VerUsuarioComponent } from './usuarios/ver-usuario/ver-usuario.component';
import { VerPostComponent } from './inicio/ver-post/ver-post.component';
import { Inicio1Component } from '../inicio1/inicio1.component';


@NgModule({
  declarations: [
    DashboardComponent,
    InicioComponent,
    NavbarComponent,
    UsuariosComponent,
    ReportesComponent,
    VerUsuarioComponent,
    SafeHtmlPipe,
    VerPostComponent,
    Inicio1Component
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
  ]
})
export class DashboardModule { }
